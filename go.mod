module gitlab.com/alikhan.murzayev/lesson-7-task

go 1.13

require (
	github.com/gocql/gocql v0.0.0-20200228163523-cd4b606dd2fb
	github.com/google/uuid v1.1.1
)
