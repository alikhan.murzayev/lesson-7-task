package main

import (
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/repository/scylladb"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/rpc/http"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/user"
	"runtime"
	"time"
)

func main() {
	defer TrackTime(time.Now(), "User extraction")

	runtime.GOMAXPROCS(runtime.NumCPU())
	err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	var (
		userQueryRepo    domain.UserQueryRepo
		postQueryRepo    domain.PostQueryRepo
		commentQueryRepo domain.CommentQueryRepo
		photoQueryRepo   domain.PhotoQueryRepo
		albumQueryRepo   domain.AlbumQueryRepo
		todoQueryRepo    domain.TodoQueryRepo
		userCommandRepo domain.UserCommandRepo
	)

	userQueryRepo = http.NewUserQueryRepo()
	postQueryRepo = http.NewPostQueryRepo()
	commentQueryRepo = http.NewCommentQueryRepo()
	albumQueryRepo = http.NewAlbumQueryRepo()
	photoQueryRepo = http.NewPhotoQueryRepo()
	todoQueryRepo = http.NewTodoQueryRepo()

	userService := user.NewService(userQueryRepo, postQueryRepo, commentQueryRepo, albumQueryRepo, photoQueryRepo, todoQueryRepo)

	userIds := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	var users []domain.User
	for i := range userIds {
		myUser, err := userService.GetUser(userIds[i])
		if err != nil {
			panic(err)
		}
		users = append(users, *myUser)
	}
	fmt.Println("Extracted users from API")

	dbConnection, err := scylladb.ScyllaDBConnectionStart()
	if err != nil {
		panic(err)
	}
	defer dbConnection.Close()

	userCommandRepo = scylladb.NewUserCommandRepo(dbConnection)

	// Store users
	err = userCommandRepo.Store(&users)
	if err != nil {
		panic(err)
	}
	fmt.Println("Added users")

	//Get users by ids
	storedUsers, err := userCommandRepo.GetByIds(userIds)
	if err != nil {
		panic(err)
	}
	fmt.Println(len(*storedUsers), "users stored")

	// Delete users by ids
	err = userCommandRepo.DeleteByIds(userIds[:len(userIds) / 2])
	if err != nil {
		panic(err)
	}

	fmt.Println("Deleted users")
	storedUsers, err = userCommandRepo.GetByIds(userIds)
	fmt.Println(len(*storedUsers), "users left")

	fmt.Println("Deleting all the users")
	_ = userCommandRepo.DeleteByIds(userIds)

}

func TrackTime(startTime time.Time, name string) {
	fmt.Printf("%s took %s\n", name, time.Since(startTime))
}
