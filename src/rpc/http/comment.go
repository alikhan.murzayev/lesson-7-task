package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
	"net/http"
)

type commentQueryRepo struct {
	baseUrl string
}

func (commentQueryRepo *commentQueryRepo) GetPostComments(postId int) (*[]domain.Comment, error) {
	commentsUrl := fmt.Sprintf(commentQueryRepo.baseUrl, postId)
	resp, err := http.Get(commentsUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var comments []domain.Comment
	err = json.Unmarshal(body, &comments)
	if err != nil {
		return nil, err
	}
	return &comments, nil

}

func NewCommentQueryRepo() domain.CommentQueryRepo {
	return &commentQueryRepo{baseUrl: config.AllConfigs.JsonPlaceholder.CommentsUrl}
}
