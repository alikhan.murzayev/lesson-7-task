package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
	"net/http"
)

type todoQueryRepo struct {
	baseUrl string
}

func (todoQueryRepo *todoQueryRepo) GetUserTodos(userId int) (*[]domain.Todo, error) {
	todosUrl := fmt.Sprintf(todoQueryRepo.baseUrl, userId)
	resp, err := http.Get(todosUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var todos []domain.Todo
	err = json.Unmarshal(body, &todos)
	if err != nil {
		return nil, err
	}
	return &todos, nil
}

func NewTodoQueryRepo() domain.TodoQueryRepo {
	return &todoQueryRepo{baseUrl: config.AllConfigs.JsonPlaceholder.TodosUrl}
}
