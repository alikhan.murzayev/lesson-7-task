package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
	"net/http"
)

type photoQueryRepo struct {
	baseUrl string
}

func (photoQueryRepo *photoQueryRepo) GetAlbumPhotos(albumId int) (*[]domain.Photo, error) {
	photosUrl := fmt.Sprintf(photoQueryRepo.baseUrl, albumId)
	resp, err := http.Get(photosUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var photos []domain.Photo
	err = json.Unmarshal(body, &photos)
	if err != nil {
		return nil, err
	}
	return &photos, nil
}

func NewPhotoQueryRepo() domain.PhotoQueryRepo {
	return &photoQueryRepo{baseUrl: config.AllConfigs.JsonPlaceholder.PhotosUrl}
}
