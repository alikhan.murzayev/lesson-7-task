package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
	"net/http"
)

type albumQueryRepo struct {
	baseUrl string
}

func (albumQueryRepo *albumQueryRepo) GetUserAlbums(userId int) (*[]domain.Album, error) {
	albumUrl := fmt.Sprintf(albumQueryRepo.baseUrl, userId)
	resp, err := http.Get(albumUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var albums []domain.Album
	err = json.Unmarshal(body, &albums)
	if err != nil {
		return nil, err
	}
	return &albums, nil

}

func NewAlbumQueryRepo() domain.AlbumQueryRepo {
	return &albumQueryRepo{baseUrl: config.AllConfigs.JsonPlaceholder.AlbumsUrl}
}
