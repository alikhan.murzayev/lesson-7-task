package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
	"net/http"
)

type postQueryRepo struct {
	baseUrl string
}

func (postQueryRepo *postQueryRepo) GetUserPosts(userId int) (*[]domain.Post, error) {
	postUrl := fmt.Sprintf(postQueryRepo.baseUrl, userId)
	resp, err := http.Get(postUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var posts []domain.Post
	err = json.Unmarshal(body, &posts)
	if err != nil {
		return nil, err
	}
	return &posts, nil
}

func NewPostQueryRepo() domain.PostQueryRepo {
	return &postQueryRepo{baseUrl: config.AllConfigs.JsonPlaceholder.PostsUrl}
}
