package domain

// Main structure
type Post struct {
	UserId   int       `json:"userId"`
	Id       int       `json:"id"`
	Title    string    `json:"title"`
	Body     string    `json:"body"`
	Comments []Comment `json:"comments,omitempty"`
}

type PostQueryRepo interface {
	GetUserPosts(userId int) (*[]Post, error)
}
