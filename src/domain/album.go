package domain

type Album struct {
	UserId int     `json:"userId"`
	Id     int     `json:"id"`
	Title  string  `json:"title"`
	Photos []Photo `json:"photos,omitempty"`
}

type AlbumQueryRepo interface {
	GetUserAlbums(userId int) (*[]Album, error)
}
