package domain

type Todo struct {
	UserId    int    `json:"userId"`
	Id        int    `json:"id"`
	Title     string `json:"title"`
	Completed bool   `json:"completed"`
}

type TodoQueryRepo interface {
	GetUserTodos(userId int) (*[]Todo, error)
}
