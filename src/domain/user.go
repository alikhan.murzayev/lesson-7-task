package domain

import (
	"github.com/google/uuid"
	"time"
)

// Main structure
type User struct {
	Uuid     string  `json:"uuid,omitempty"`
	Id       int     `json:"id"`
	Name     string  `json:"name"`
	Username string  `json:"username"`
	Email    string  `json:"email"`
	Address  Address `json:"address,omitempty"`
	Phone    string  `json:"phone,omitempty"`
	Website  string  `json:"website,omitempty"`
	Company  Company `json:"company,omitempty"`

	Albums []Album `json:"albums,omitempty"`
	Posts  []Post  `json:"posts,omitempty"`
	Todos  []Todo  `json:"todos,omitempty"`

	LastUpdated time.Time `json:"last_updated"`
}

// User's additional structures
type Address struct {
	Street  string `json:"street"`
	Suite   string `json:"suite,omitempty"`
	City    string `json:"city,omitempty"`
	ZipCode string `json:"zipcode,omitempty"`
	Geo     Geo    `json:"geo,omitempty"`
}

type Geo struct {
	Lat string `json:"lat,omitempty"`
	Lng string `json:"lng,omitempty"`
}

type Company struct {
	Name        string `json:"name"`
	CatchPharse string `json:"catchPhrase,omitempty"`
	Bs          string `json:"bs,omitempty"`
}

// Generate unique User uuid
func (user *User) GenerateUid() {
	user.Uuid = uuid.New().String()
}

// Set current timestamp
func (user *User) SetTimestamp() {
	user.LastUpdated = time.Now()
}

// Empty fields
func (user *User) Empty() {
	user.Posts = nil
	user.Albums = nil
	user.Todos = nil
}

type UserQueryRepo interface {
	GetUser(userId int) (*User, error)
}

type UserCommandRepo interface {
	Store(*[]User) error
	GetByIds([]int) (*[]User, error)
	DeleteByIds([]int) error
}
