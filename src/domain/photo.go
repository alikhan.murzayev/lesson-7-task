package domain

type Photo struct {
	AlbumId      int    `json:"albumId"`
	Id           int    `json:"id"`
	Title        string `json:"title"`
	Url          string `json:"url"`
	ThumbnailUrl string `json:"thumbnailUrl,omitempty"`
}

type PhotoQueryRepo interface {
	GetAlbumPhotos(albumId int) (*[]Photo, error)
}
