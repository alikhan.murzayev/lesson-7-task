package scylladb

import (
	"github.com/gocql/gocql"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/config"
	"time"
)

func ScyllaDBConnectionStart() (*gocql.Session, error) {
	cluster := gocql.NewCluster(config.AllConfigs.ScyllaDB.ConnectionIP...)
	cluster.Consistency = gocql.Quorum
	cluster.Keyspace = config.AllConfigs.ScyllaDB.KeySpace
	cluster.ProtoVersion = 4
	cluster.SocketKeepalive = 10 * time.Second

	session, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	return session, nil
}
