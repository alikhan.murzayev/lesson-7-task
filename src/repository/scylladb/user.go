package scylladb

import (
	"encoding/json"
	"fmt"
	"github.com/gocql/gocql"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"strconv"
	"time"
)

type userCommandRepo struct {
	dbConnection *gocql.Session
}

func NewUserCommandRepo(connection *gocql.Session) domain.UserCommandRepo {
	return &userCommandRepo{dbConnection: connection}
}

func (userCommandRepo *userCommandRepo) Store(users *[]domain.User) error {
	var batchSize = 1000
	batch := userCommandRepo.dbConnection.NewBatch(gocql.LoggedBatch)

	stmt := "INSERT INTO users (id, user_id, name, username, email, address, phone, website, company, last_updated) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	for i, user := range *users {
		user.GenerateUid()
		user.SetTimestamp()
		addressBytes, err := json.Marshal(user.Address)
		if err != nil {
			return err
		}

		companyBytes, err := json.Marshal(user.Company)
		if err != nil {
			return err
		}

		batch.Query(
			stmt,
			user.Uuid,
			user.Id,
			user.Name,
			user.Username,
			user.Email,
			string(addressBytes),
			user.Phone,
			user.Website,
			string(companyBytes),
			user.LastUpdated)

		if (i+1)%batchSize == 0 {
			err := userCommandRepo.dbConnection.ExecuteBatch(batch)
			if err != nil {
				return err
			}
			batch = userCommandRepo.dbConnection.NewBatch(gocql.LoggedBatch)
		}
	}
	err := userCommandRepo.dbConnection.ExecuteBatch(batch)
	if err != nil {
		return err
	}
	return nil
}

func (userCommandRepo *userCommandRepo) GetByIds(ids []int) (*[]domain.User, error) {
	var batchSize = 1000
	var users []domain.User

	for i := 0; i < len(ids); i += batchSize {
		startIndex := i
		endIndex := min(len(ids), i+batchSize)
		stmt := fmt.Sprintf("SELECT * FROM user_view WHERE user_id in (%s)", IntsToString(ids[startIndex:endIndex], ", "))
		iter := userCommandRepo.dbConnection.Query(stmt).Iter()

		mapper := map[string]interface{}{}
		for iter.MapScan(mapper) {
			var (
				address domain.Address
				company domain.Company
			)
			err := json.Unmarshal([]byte(mapper["address"].(string)), &address)
			if err != nil {
				return nil, err
			}

			err = json.Unmarshal([]byte(mapper["company"].(string)), &company)
			if err != nil {
				return nil, err
			}

			user := domain.User{
				Uuid:        mapper["id"].(gocql.UUID).String(),
				Id:          mapper["user_id"].(int),
				Name:        mapper["name"].(string),
				Username:    mapper["username"].(string),
				Email:       mapper["email"].(string),
				Address:     address,
				Phone:       mapper["phone"].(string),
				Website:     mapper["website"].(string),
				Company:     company,
				LastUpdated: mapper["last_updated"].(time.Time),
			}
			users = append(users, user)
			mapper = map[string]interface{}{}
		}
	}
	return &users, nil
}

func (userCommandRepo *userCommandRepo) DeleteByIds(ids []int) error {
	var batchSize = 1000
	batch := userCommandRepo.dbConnection.NewBatch(gocql.LoggedBatch)
	stmt := "DELETE FROM users WHERE id = ? AND user_id = ?"
	users, err := userCommandRepo.GetByIds(ids)
	if err != nil {
		return err
	}
	for i := range *users {
		batch.Query(stmt, (*users)[i].Uuid, (*users)[i].Id)

		if (i+1)%batchSize == 0 {
			err = userCommandRepo.dbConnection.ExecuteBatch(batch)
			if err != nil {
				return err
			}
			batch = userCommandRepo.dbConnection.NewBatch(gocql.LoggedBatch)
		}
	}
	err = userCommandRepo.dbConnection.ExecuteBatch(batch)
	if err != nil {
		return err
	}
	return nil
}

// Helper functions
func IntsToString(nums []int, delimiter string) string {
	var result string
	for i := 0; i < len(nums)-1; i++ {
		result += strconv.Itoa(nums[i]) + delimiter
	}
	if len(nums) > 0 {
		result += strconv.Itoa(nums[len(nums)-1])
	}
	return result
}

func RepeatQuestionSign(n int) string {
	var result string
	for i := 0; i < n-1; i++ {
		result += "?, "
	}
	if n > 0 {
		result += "?"
	}
	return result
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
