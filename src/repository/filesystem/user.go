package filesystem

import (
	"encoding/json"
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"io/ioutil"
)

type userCommandRepo struct {
	userData domain.User
	filePath string
}

func (userCommandRepo *userCommandRepo) StoreJson() error {
	userBytes, err := json.MarshalIndent(userCommandRepo.userData, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(userCommandRepo.filePath, userBytes, 0644)
	if err != nil {
		return err
	}
	return nil
}

func NewUserCommandRepo(userData domain.User, filePath string) domain.UserCommandRepo {
	return &userCommandRepo{
		userData: userData,
		filePath: filePath,
	}
}
