package config

import (
	"encoding/json"
	"fmt"
	"os"
)

//////////////////////////////
// Config structures & vars //
//////////////////////////////

type Configs struct {
	ScyllaDB        ScyllaDBConfig        `json:"scylla_db"`
	JsonPlaceholder JsonPlaceholderConfig `json:"jsonplaceholder"`
}

type ScyllaDBConfig struct {
	ConnectionIP []string `json:"connection_ip"`
	KeySpace     string   `json:"key_space"`
}

type JsonPlaceholderConfig struct {
	UserUrl     string `json:"user_url"`
	PostsUrl    string `json:"posts_url"`
	CommentsUrl string `json:"comments_url"`
	AlbumsUrl   string `json:"albums_url"`
	PhotosUrl   string `json:"photos_url"`
	TodosUrl    string `json:"todos_url"`
}

var AllConfigs *Configs

//////////////////////
// Config functions //
//////////////////////

// Get configs
func GetConfigs() error {
	var filePath string
	if os.Getenv("config") != "" {
		filePath = "config"
	} else {
		currentDir, err := os.Getwd()
		if err != nil {
			fmt.Println("get current dir error:", err)
		}
		filePath = currentDir + "/src/config/config.json"
	}

	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&AllConfigs)
	if err != nil {
		return err
	}

	return nil
}
