package user

import (
	"gitlab.com/alikhan.murzayev/lesson-7-task/src/domain"
	"sync"
)

type Service interface {
	GetUser(userId int) (*domain.User, error)
	GetUserPhotos(albumId int) (*[]domain.Photo, error)
	GetUserAlbums(userId int) (*[]domain.Album, error)
	GetPostComments(postId int) (*[]domain.Comment, error)
	GetUserPosts(userId int) (*[]domain.Post, error)
	GetUserTodos(userId int) (*[]domain.Todo, error)
}

func NewService(userQueryRepo domain.UserQueryRepo, postQueryRepo domain.PostQueryRepo, commentQueryRepo domain.CommentQueryRepo, albumQueryRepo domain.AlbumQueryRepo, photoQueryRepo domain.PhotoQueryRepo, todoQueryRepo domain.TodoQueryRepo) Service {
	return &service{
		userQueryRepo:    userQueryRepo,
		postQueryRepo:    postQueryRepo,
		commentQueryRepo: commentQueryRepo,
		albumQueryRepo:   albumQueryRepo,
		photoQueryRepo:   photoQueryRepo,
		todoQueryRepo:    todoQueryRepo,
	}
}

type service struct {
	userQueryRepo    domain.UserQueryRepo
	postQueryRepo    domain.PostQueryRepo
	commentQueryRepo domain.CommentQueryRepo
	albumQueryRepo   domain.AlbumQueryRepo
	photoQueryRepo   domain.PhotoQueryRepo
	todoQueryRepo    domain.TodoQueryRepo
}

func (service *service) GetUser(userId int) (*domain.User, error) {
	user, err := service.userQueryRepo.GetUser(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, 2)

	// extract todos concurrently
	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, errChan chan<- error) {
		defer waitGroup.Done()
		todos, err := service.GetUserTodos(user.Id)
		if err != nil {
			errChan <- err
			return
		}
		user.Todos = make([]domain.Todo, len(*todos))
		copy(user.Todos, *todos)
	}(&waitGroup, user, errChan)

	// extract posts concurrently
	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, errChan chan<- error) {
		defer waitGroup.Done()
		posts, err := service.GetUserPosts(userId)
		if err != nil {
			errChan <- err
			return
		}
		user.Posts = make([]domain.Post, len(*posts))
		copy(user.Posts, *posts)
	}(&waitGroup, user, errChan)

	// extract albums concurrently
	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, errChan chan<- error) {
		defer waitGroup.Done()
		albums, err := service.GetUserAlbums(user.Id)
		if err != nil {
			errChan <- err
			return
		}
		user.Albums = make([]domain.Album, len(*albums))
		copy(user.Albums, *albums)
	}(&waitGroup, user, errChan)

	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return user, nil
}

func (service *service) GetUserPhotos(albumId int) (*[]domain.Photo, error) {
	photos, err := service.photoQueryRepo.GetAlbumPhotos(albumId)
	if err != nil {
		return nil, err
	}
	return photos, nil
}

func (service *service) GetUserAlbums(userId int) (*[]domain.Album, error) {
	albums, err := service.albumQueryRepo.GetUserAlbums(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, len(*albums))

	for i, _ := range *albums {
		waitGroup.Add(1)
		go func(waitGroup *sync.WaitGroup, album *domain.Album, errChan chan<- error) {
			defer waitGroup.Done()
			photos, err := service.GetUserPhotos(album.UserId)
			if err != nil {
				errChan <- err
				return
			}
			album.Photos = make([]domain.Photo, len(*photos))
			copy(album.Photos, *photos)
		}(&waitGroup, &(*albums)[i], errChan)
	}
	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return albums, nil
}

func (service *service) GetPostComments(postId int) (*[]domain.Comment, error) {
	comments, err := service.commentQueryRepo.GetPostComments(postId)
	if err != nil {
		return nil, err
	}
	return comments, nil
}

func (service *service) GetUserPosts(userId int) (*[]domain.Post, error) {
	posts, err := service.postQueryRepo.GetUserPosts(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, len(*posts))
	for i, _ := range *posts {
		waitGroup.Add(1)
		go func(waitGroup *sync.WaitGroup, post *domain.Post, errChan chan<- error) {
			defer waitGroup.Done()
			comments, err := service.GetPostComments(post.Id)
			if err != nil {
				errChan <- err
				return
			}
			post.Comments = make([]domain.Comment, len(*comments))
			copy(post.Comments, *comments)
		}(&waitGroup, &(*posts)[i], errChan)

	}
	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return posts, nil
}

func (service *service) GetUserTodos(userId int) (*[]domain.Todo, error) {
	todos, err := service.todoQueryRepo.GetUserTodos(userId)
	if err != nil {
		return nil, err
	}
	return todos, nil
}
